#include "App.h"
//Esto lo comentamos porque parece que ya no hace falta, esta incluido en FreeRTOS.
#include <NXP/crp.h>//esto lo agregue, no se por que todos los otros programas no necesitan agregar esto explicitamente y este si, pero  por ahora hacer esto funciona.
__CRP const unsigned int CRP_WORD = CRP_NO_CRP ;

#include "board.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "DR_pinsel.h"
#include "DR_adc.h"

volatile int Demora_TX;

extern uint16_t	Vlr_Mdd[4];

//extern uint8_t flag_ADC;
extern xSemaphoreHandle sem_FlagListoADC;

void maquina_estados (void)
{

	/*******************************************************************
	 * Declaración de variables para lectura de valor analógico de LDR's.
	 * Se les asigna el valor de lectura del LDR correspondiente.
	 * *****************************************************************/

	uint16_t r_ii = 0;
	uint16_t r_si = 0;
	uint16_t r_id = 0;
	uint16_t r_sd = 0;
	static uint16_t posicionActual_v = 1500;
	static uint16_t posicionActual_h = 1500;


	r_ii = Vlr_Mdd[0];	//valor medido, a cada posicion del vector se le carga el valor medido por el ADC
	r_si = Vlr_Mdd[1];
	r_id = Vlr_Mdd[2];
	r_sd = Vlr_Mdd[3];

	//este if creo que esta de mas.
	if  ( (r_ii != 0) && (r_si != 0) && (r_id != 0) && (r_sd != 0) )//si los 4 son distintos de cero
	{
		uint16_t prom_S = (uint16_t)(r_si + r_sd) / 2; //Promedio de los LDR's superiores.
		uint16_t prom_I = (uint16_t)(r_ii + r_id) / 2; //Promedio de los LDR's inferiores.
		uint16_t prom_Izq = (uint16_t)(r_si + r_ii) / 2; //Promedio de los LDR's izquierdos.
		uint16_t prom_Der = (uint16_t)(r_sd + r_id) / 2; //Promedio de los LDR's derechos.



		uint16_t priMov_v = 0;
		priMov_v = prioridad_mov(prom_I, prom_S);

		uint16_t rectMov_v = 0;
		rectMov_v = rectaMovimiento(priMov_v);


		if(rectMov_v < 1500)
		{
			posicionActual_v = posicionActual_v - 5;
		}

		if(rectMov_v > 1500)
		{
			posicionActual_v = posicionActual_v + 5;
		}


		if (posicionActual_v < 500)
			posicionActual_v = 500;
		if (posicionActual_v > 2400)
			posicionActual_v = 2400;

		uint16_t priMov_h = 0;
		priMov_h = prioridad_mov(prom_Der, prom_Izq);

		uint16_t rectMov_h = 0;
		rectMov_h = rectaMovimiento(priMov_h);

		if ( posicionActual_v < 1500 )
		{
			if(rectMov_h < 1500)
			{
				posicionActual_h = posicionActual_h + 5;
			}

			if(rectMov_h > 1500)
			{
				posicionActual_h = posicionActual_h - 5;
			}
		}
		else
		{
			if(rectMov_h < 1500)
			{
				posicionActual_h = posicionActual_h - 5;
			}

			if(rectMov_h > 1500)
			{
				posicionActual_h = posicionActual_h + 5;
			}

		}
		if (posicionActual_h < 500)
			posicionActual_h = 500;
		if (posicionActual_h > 2400)
			posicionActual_h = 2400;
	}
}

/*******************************************************************************
 * @fn static void task_Principal ( void )
 * @details Tarea principal, contiene a la maquina de estados.
 * @details No Portable
 * @param 	void
 * @return 	void.
*******************************************************************************/
static void task_Principal(void * a)
{

while (1) {

		if( xSemaphoreTake(sem_FlagListoADC, portMAX_DELAY) )
		{
			maquina_estados();
			vTaskDelay(25 / portTICK_RATE_MS);
		}
	}
}

static void initHardware(void)
{
    SystemCoreClockUpdate();

    SetPINSEL (ADC_CH0,FUNCION_1);		// CHN0
    SetPINSEL (ADC_CH1,FUNCION_1);		// CHN1
    SetPINSEL (ADC_CH2,FUNCION_1);		// CHN2
    SetPINSEL (ADC_CH3,FUNCION_1);		// CHN3
    SetPINSEL (ADC_CH4,FUNCION_3);		// CHN4

    Inic_ADC ( );

    Board_Init();
}

int main(void)
{
	initHardware();

	sem_FlagListoADC = xSemaphoreCreateMutex();

	if(sem_FlagListoADC)
	{
		xTaskCreate(task_Principal, (const char *)"task_Principal", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);
		vTaskStartScheduler();
	}

	while ( 1 ){
	}

    return 0 ;
}
