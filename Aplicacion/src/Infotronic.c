#include "Infotronic.h"

volatile int Demora_PWM;
extern uint16_t	Vlr_Mdd [3];
volatile uint16_t angle_h = 1500;
volatile uint16_t angle_v = 1500;

uint16_t rectaMovimiento ( uint16_t difProm)
{
	uint16_t recMov = 0;

	float ordenada 	= 1000;
	float pendiente	= 0;
	pendiente = (float)1000/(float)8192;

	recMov = (uint16_t) (pendiente*difProm + ordenada);

	return recMov;
	//lo casteamos porque la cuenta da un float
}

uint16_t prioridad_mov ( uint16_t promA, uint16_t promB )//despues cambiamos el nombre a difProm(diferencia primedios)
{
	uint16_t priMov = 0;

	priMov = (4096 + promA - promB);

	return priMov;
	//4096 es el valor mmaximo que admite el sensor al ser iluminado al 100% y es el valor en que desplazamos al rango.
}
